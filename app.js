const fetch = require("node-fetch");
const FoodItem = require("./food.js");

async function getRecommendation(input) {
  const heroku_url = "https://profit-food-recommendation.herokuapp.com/";
  const results = [];
  input = JSON.stringify(input);
  console.log("input: " + input);

  //send post request to heroku server using fetch
  const response = await fetch(heroku_url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: input,
  });
  const data = await response.json();
  // transfer data to results array

  for (let i = 0; i < Object.keys(data).length; i++) {
    results.push(data[i]);
  }
  console.log(results);
  return results;
}


async function getCalories(foods) {
  const url = "https://api.calorieninjas.com/v1/nutrition?query=";
  const key = "JXKxU70Kbo7m0hMwp8QZPg==TSFNr4lJizKe5ZX3";
  try {
    const promises = foods.map((food) => {
      return (_) => 
      fetch(url + food, {
        method: "GET",
        headers: {
          "X-Api-Key": key,
        },
      }).then((response) => response.json());
     
    });
    const results = await Promise.all(promises.map((promise) => promise()));
  
   
    finalFoodItems = [];
  
    results.forEach((item) => {
      data = item;
      console.log(data["items"]);
      finalFoodItem = new FoodItem("", 0.0, 0.0, 0.0, 0.0, 0.0);
      if (data["items"].length > 0) {
        for (let i = 0; i < data["items"].length; i++) {
          finalFoodItem.addName(data["items"][i]["name"]);
          finalFoodItem.addCalories(data["items"][i]["calories"]);
          finalFoodItem.addServingSize(data["items"][i]["serving_size_g"]);
          finalFoodItem.addCarbs(data["items"][i]["carbohydrates_total_g"]);
          finalFoodItem.addProtein(data["items"][i]["protein_g"]);
          finalFoodItem.addFats(data["items"][i]["fat_total_g"]);
        }
        finalFoodItem.updateCalories(data["items"].length);
        finalFoodItem.updateServingSize(data["items"].length);
        finalFoodItem.updateCarbs(data["items"].length);
        finalFoodItem.updateProtein(data["items"].length);
        finalFoodItem.updateFats(data["items"].length);
        finalFoodItems.push(finalFoodItem);
      }
    });

    return finalFoodItems;
  } catch (error) {
    console.log(error);
  }

}

//export getresults and getrecommendation functions
module.exports = {
  getRecommendation,
  getCalories,
};
