class FoodItem {
    constructor(name, calories, serving_size_g, carbohydrates_total_g, fat_total_g, protein_g) {
        this.name = name;
        this.calories = calories;
        this.serving_size_g = serving_size_g;
        this.carbohydrates_total_g = carbohydrates_total_g;
        this.fat_total_g = fat_total_g;
        this.protein_g = protein_g;


    }
    addName(name) {

        this.name += " " + name;
    }
    addCalories(calories) {

        this.calories += calories;
    }
    addServingSize(serving_size_g) {
        this.serving_size_g += serving_size_g;
    }

    addCarbs(carbohydrates_total_g) {
        this.carbohydrates_total_g += carbohydrates_total_g;
    }

    addProtein(protein_g) {
        this.protein_g += protein_g;
    }

    addFats(fat_total_g) {
        this.fat_total_g += fat_total_g;
    }


    //empty constructor
    // constructor() {
    // this.name = '';
    // this.calories = 0;
    // this.serving_size_g = 0;

    // }



    updateCalories(length) {
        this.calories = (this.calories / length) * 1.3;


    }
    updateCarbs(length) {
        this.carbohydrates_total_g = (this.carbohydrates_total_g / length) * 1.3;
    }
    updateFats(length) {
        this.fat_total_g = (this.fat_total_g / length) * 1.3;
    }
    updateProtein(length) {
        this.protein_g = (this.protein_g / length) * 1.3;
    }

    updateServingSize(num) {
        this.serving_size_g = 100;
    }


}




module.exports = FoodItem;