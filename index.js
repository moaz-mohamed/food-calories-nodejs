const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3000;

const { getRecommendation, getCalories } = require('./app.js');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}!`);

});

app.post('/', async (req, res) => {
    console.log("request body" + req.body);
    results = await getRecommendation(req.body);
    finalFoodItems = await getCalories(results);
    res.send(finalFoodItems);
})



